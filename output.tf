output "network_interface" {
  value = google_compute_address.web-static.address
}

output "image_name" {
  value = data.google_compute_image.web_image.name
}

output "server_name" {
  value = google_compute_instance.web_server.name
}