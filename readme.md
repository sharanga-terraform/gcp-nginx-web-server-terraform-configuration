**GCP Nginx Web Server Terraform Configuration**

This repository contains terraform configuration files to configure an GCP compute instance running nginx web server. Following is the outline of the configuration:
1. Define GCP provider along and add service account json file to the root folder
2. Create a static IP with standard network tier
3. Create firewall rule to allow port 22,80,443
4. Fetch & deploy the latest ubuntu 22 server image and call startup script to install and enable nginx
5. Define variables and outputs


**File Structure:**
- _main.tf_ consists of providers and modules
-  _variable.tf_ contains a set of variables which can be changed as per needs of your configuration.
-  _output.tf_ contains the output a user need to show when applying the terraform configuration.


**Terraform Commands:**
1. terraform init
2. terraform validate
3. terraform plan
4. terraform apply -auto-approve
