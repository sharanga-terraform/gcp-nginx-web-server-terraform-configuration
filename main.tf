###############################################################################
# Declaring Provider
################################################################################

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.47.0"
    # Latest provider verion: https://registry.terraform.io/providers/hashicorp/google/latest
    }
  }
}

provider "google" {
  credentials = "keys.json"
  project     = var.project
  region      = var.region
  zone        = var.zone
}

################################################################################
# Declaring Data Source
################################################################################

data "google_compute_image" "web_image" {
  family  = var.image[0]
  project = var.image[1]
}

################################################################################
# Declaring Resources
################################################################################

resource "google_compute_network" "web_network" {
  name = "web-network"
}

resource "google_compute_firewall" "web" {
  name          = "web-firewall"
  network       = google_compute_network.web_network.id
  source_ranges = [var.cidr_block]

  allow {
    protocol = "tcp"
    ports    = [var.ports[0], var.ports[1]]
  }

  target_tags = ["web-server"]
}

resource "google_compute_project_default_network_tier" "default" {
  network_tier = var.tier
}

# resource "google_compute_address" "web-static" {
#   name       = var.address_name
#   depends_on = [google_compute_firewall.web]
# }

resource "google_compute_instance" "web_server" {
  name                      = var.server_name
  machine_type              = var.machine_type
  allow_stopping_for_update = true

  tags = ["web-server"]

  boot_disk {
    initialize_params {
      image = data.google_compute_image.web_image.id
      size  = var.disk_size
    }
  }
  network_interface {
    network = google_compute_network.web_network.id # Enables internal IP address
    access_config {                                 # Enables external IP address
      nat_ip = google_compute_address.web-static.address
    }
  }

  metadata_startup_script = file("startup-script.sh")

  metadata = {
  block-project-ssh-keys = true
  }


  service_account {
    email  = var.email
    scopes = ["cloud-platform"]
  }
}