variable "project" {
  description = "GCP project name"
  default     = "my-test-project-370917"
}

variable "region" {
  description = "GCP region"
  default     = "us-west1"
}

variable "zone" {
  description = "GCP resouce zone"
  default     = "us-west1-a"
}

variable "machine_type" {
  description = "Type of machine"
  default     = "f1-micro"
}

variable "email" {
  description = "Service account email"
  default     = "someemail@domain.com"
}

variable "network" {
  description = "Network Name"
  default     = "default"
}

variable "cidr_block" {
  description = "CIDR blocks for firewall source range"
  default     = "0.0.0.0/0"
}

variable "ports" {
  description = "Firewall ports"
  default     = ["22", "80", "443"]
}

variable "address_name" {
  description = "Name of the static IP of the instance"
  default = "web-server-ip"
}

variable "tier" {
    description = "Network tier for static IP"
    default = "STANDARD"
}

variable "server_name" {
    description = "Name of the server"
    default = "web-server"
}

variable "image" {
  description = "Boot disk to be used in the instance"
  default     = ["ubuntu-minimal-2204-lts", "ubuntu-os-cloud"]
}

variable "disk_size" {
  description = "Size of boot image"
  default     = "10"
}